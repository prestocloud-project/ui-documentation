About PrEstoCloud 
========

In the current Handbook, description of the PrEstoCloud Online platform is provided. The PrEstoCloud online platform consists of the main web-based
platform developed by PrEstoCloud project consortium, and allows the design, deployment and management of secure and elastic-by design services that can be deployed in multi-cloud environments.