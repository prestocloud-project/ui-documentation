PrEstoCloud walkthrough
========
Create Account
--------
- When a user access the PrEstoCloud site (http://prestocloud-project.eu/) the following screen is shown:

.. image:: assets/intro_1.png

- Click on "Register" button.

.. image:: assets/intro_2.png

- Provide account information.

.. image:: assets/intro_3.png

- Click on "Create Account" button.

.. image:: assets/intro_4.png

- Go to your email and click on PrEstoCloud account information link.

.. image:: assets/intro_5.png

- Your Account has successfully verified.

.. image:: assets/intro_6.png

- Now  you can login to your account.

.. image:: assets/intro_7.png
