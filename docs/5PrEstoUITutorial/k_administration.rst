###############################################################
Administration
###############################################################


.. note::
  This section is still under work

========
Overview
========
xxx


========
User Management
========
Create Users
--------


.. note::
  This section is still under work

- Visit the PrEstoCloud Platform `PrEstoCloud Platform Webpage`_

.. _Unicorn Platform Webpage: https://prestocloud.euprojects.net/

- When a user access the PrEstoCloud the following screen is shown:

.. image:: assets/login.PNG

- Click on login button.

.. image:: assets/login.PNG

- Click on "Create Account" button.

.. image:: assets/login.PNG

- Provide account information and click on "CREATE ACCOUNT" button.

.. image:: assets/login.PNG

- Go to your email and click on PrEstoCloud account information link.

.. image:: assets/login.PNG

- You have successfully Created an New Account.



