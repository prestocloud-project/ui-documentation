###############################################################
Getting started with PrEstoCloud Dashboard
###############################################################


.. note::
  This section is still under work

========
Register and Login
========


Login
------------



- Click the <Log in> button .

.. image:: assets/login.PNG

- Provide your login credentials and click the <SIGN IN> button.

.. image:: assets/login.PNG

- Upon successful authentication the following screen will be presented.

.. image:: assets/dash1.PNG


Logout
----------

- In order to perform logout  click the <Log-out > Button.

.. image:: assets/logout.png

- Upon successful logout the following screen will be presented.

.. image:: assets/login.PNG



Create Users
--------


.. note::
  This section is still under work

- Visit the PrEstoCloud Platform `PrEstoCloud Platform Webpage`_

.. _Unicorn Platform Webpage: https://prestocloud.euprojects.net/

- When a user access the PrEstoCloud the following screen is shown:

.. image:: assets/login.PNG

- Click on login button.

.. image:: assets/login.PNG

- Click on "Create Account" button.

.. image:: assets/login.PNG

- Provide account information and click on "CREATE ACCOUNT" button.

.. image:: assets/login.PNG

- Go to your email and click on PrEstoCloud account information link.

.. image:: assets/login.PNG

- You have successfully Created an New Account.




========
Dashboard Main View
========

* Through the dashboard, an overview of the components, applications and deployed application instance is provided.
* In PrEstoCloud, applications are described as microservices composed by smaller components.

* Also, an overview of the available and used, aggregated cloud resources of the user is provided.

.. image:: assets/dash1.PNG





=============================
Application Elasticity
=============================
- From the application instance list, the user must select the "Elasticity Policies" option for the deployed application, in order to configure how the application scales.
- By selecting the appropriate function, user can to aggregate the monitoring results in various ways.
- For the monitored parameter we select the metric and it's dimension from appropriate lists.
- An operand shall be added to the policy and the threshold that the policy shall confirm to.
- The period field is used to set the size of the time window that the metric values are collected and aggregated for the policy enforcement.

.. image:: assets/ela_policies create.PNG

- On the scaling action we can select the component to scale in or scale out, and the number of workers to scale.
   - After a scaling action is performed, some time is needed for having the component workers deployed. For this reason we should ensure that for this period we don't fire additional scaling actions.
   - This is done through the "Inertia" field that is used to define the time in minutes that after a scaling action is done,no further action is performed.
- Multiple scaling actions can be added.
- The policy can be saved and will be enforced to the application within few seconds.

.. image:: assets/ela_policies create.PNG

- In this example we had initially only one worker of the WordPress component.
- But due to the scaling rule, an additional node has been created.
   - A load balancer had been already deployed from the initial deployment since we had defined that this component might need multiple workers.
   - The scaling action is visible to the user through the logs and the number on workers in the "WordPress" node in the graphs.

.. image:: assets/scaled.PNG


==========
Platform Usage Video
==========

This screencast can also help you get started:

.. raw:: html

    <div style="text-align: center; margin-bottom: 2em;">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/faCqx9EGRAo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

