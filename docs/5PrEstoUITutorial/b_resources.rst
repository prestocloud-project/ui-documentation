###############################################################
Managing Cloud Resources
###############################################################


.. note::
  This section is still under work

========
Overview
========
xxx



========
Manage Cloud Resources
========
- Each user of PrEstoCloud shall add cloud resources in compatible cloud providers in order to allow the deployment of an application
- OpenStack, Amazon AWS and Google Cloud are supported
- Appropriate forms for of the each cloud providers are available;

  + OpenStack
.. image:: assets/addresource_openstack.PNG

+ Amazon AWS
.. image:: assets/addresource_aws.PNG

+ Google Cloud
.. image:: assets/addresource_google.PNG

========
Manage Your Keys
========

.. note::
  This section is still under work

